<?php

namespace Drupal\fb_messenger_bot\Conversation;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\fb_messenger_bot\Entity\BotConversation;

/**
 * Class ConversationFactory.
 *
 * @package Drupal\fb_messenger_bot\Conversation
 */
class ConversationFactory implements ConversationFactoryInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * ConversationFactory constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getConversation($uid) {
    // Check for an active, incomplete conversation.
    $cid = $this->getActiveConversationId($uid);
    if ($cid && $conversation = BotConversation::load($cid)) {
      return $conversation;
    }

    // If there is no active conversation, create a new one.
    $conversation = BotConversation::create(['uid' => $uid]);
    $conversation->save();
    return $conversation;
  }

  /**
   * Check for an active conversation with a given uid.
   *
   * @param string $uid
   *   The conversation uid.
   *
   * @return int|null
   *   The query results.
   */
  protected function getActiveConversationId($uid) {
    try {
      $query = $this->entityTypeManager
        ->getStorage('fb_messenger_bot_conversation')
        ->getQuery()
        ->condition('uid', $uid)
        ->condition('complete', BotConversationInterface::INCOMPLETE)
        ->accessCheck(FALSE)
        ->sort('created')
        ->range(0, 1);
      $ids = $query->execute();
    } catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      return NULL;
    }
    foreach ($ids as $id) {
      return $id;
    }
  }

}
